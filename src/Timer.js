import React, { useEffect, useRef, useState } from "react";

const Timer = (props) => {
  const [timerValue, setTimerValue] = useState(0);
  const [timer, setTimer] = useState(null);
  const timerStarted = useRef();

  const resetTimer = () => {
    clearInterval(timer);
    setTimer(null);
    setTimerValue(0);
  };

  useEffect(() => {
    return () => {
      resetTimer();
      alert("Timer unmounted");
    };
  }, []);

  const startTimer = () => {
    if (timer) return;
    setTimer(setInterval(updateTimer, 1000));
  };

  useEffect(() => {
    if (props.timerStarted !== timerStarted.current) {
      if (props.timerStarted) {
        startTimer();
      } else {
        resetTimer();
      }
      timerStarted.current = props.timerStarted;
    }
  }, [props.timerStarted]);

  const updateTimer = () => setTimerValue((timerValue) => timerValue + 1);

  return props.timerStarted && <div>Value: {timerValue}</div>;
};

export default Timer;