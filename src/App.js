import React, { useEffect, useState } from "react";
import "./App.css";
import Timer from "./Timer";

const App = () => {
  const [timerMounted, setTimerMounted] = useState(false);
  const [timerStarted, setTimerStarted] = useState(false);

  useEffect(() => {
    console.log("Application mounted");
  }, []);

  useEffect(() => {
    if (timerStarted) alert("Timer started");
  }, [timerStarted]);

  const changeTimerStatus = () => setTimerStarted(timerStarted => !timerStarted);

  const addTimer = () => {
    setTimerMounted(!timerMounted);
    setTimerStarted(timerMounted ? false : timerStarted);
  }

  return (
    <div className="App">
      <h1>Timer</h1>
      <button onClick={addTimer}>
        {timerMounted ? "REMOVE TIMER" : "ADD Timer"}
      </button>
      <br />
      <br />
      {timerMounted && (
        <>
          <button onClick={changeTimerStatus}>
            {timerStarted ? "STOP" : "START"}
          </button>
          <br />
          <br />
          <Timer timerStarted={timerStarted} />
        </>
      )}
    </div>
  );
};

export default App;
